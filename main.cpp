//
//  main.cpp
//  Challenge#7
//
//  Created by Panpech Pothong on 2/18/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//


#include <iostream>
#include "strvar.h"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
    using namespace std;
    conversation(30);
    cout << "End of Demo.\n";
    return 0;
}

//Demo function
void conversation(int max_name_size)
{
    using namespace std;
    using namespace strvarken;
    
    StringVar your_name(max_name_size), our_name("Borg"),name1(max_name_size),name2(max_name_size),name3(max_name_size);
    
    cout << "What is your name?\n";
    your_name.input_line(cin);
    cout << "We are " << our_name << endl;
    cout << "We will meet again " << your_name << endl;
    cout << your_name.char_one(1)<<endl;
    your_name.set_char(4, 't');
    cout << your_name<<endl;
    cout<< your_name.copy_piece(2, 3)<<endl;
    
    name1.input_line(cin);
    name2.input_line(cin);
    name1==name2;
    cout<<name1+name2<<endl;
    cin>>name3;
    cout<<name3<<endl;
}
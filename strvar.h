//
//  strvar.h
//  Challenge#7
//
//  Created by Panpech Pothong on 2/18/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//

#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace strvarken
{
    class StringVar
    {
    public:
        StringVar(int size);
        //Constructor when size of string is specified.
        StringVar();
        //Constructor with default size of 100.
        StringVar(const char a[]);
        //Precondition: The array a contains characters and ‘\0′.
        //Constructor using array as values.
        StringVar(const StringVar& string_object);
        //Copy Constructor.
        ~StringVar();
        //Destructor, returns memory to heap.
        int length() const;
        //Returns the length of the current string.
        void input_line(istream& ins);
        //Precondition: if ins is a file input stream attached to a file.
        //The next text up to ‘\n’, or the capacity of the stringvar is copied.
        friend ostream& operator <<(ostream& outs,const StringVar& the_string);
        //Precondition: if outs is a file it has been attached to a file.
        //Overloads the << operator to allow output to screen
        friend istream& operator>>(istream& input,const StringVar& the_string);
        
        char char_one(int posi);
        void set_char(int posi,char replace);
        string copy_piece(int posi,int end);
        friend bool operator==(StringVar, StringVar);
        friend StringVar operator + (StringVar , StringVar );
    private:
        char *value; //pointer to dynamic array that holds the string value.
        int max_length; //declared maximum length of the string.
    };
}//strvarken
#endif //STRVAR_H

//
//  strvar.cpp
//  Challenge#7
//
//  Created by Panpech Pothong on 2/18/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//



//strvar.cpp

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
    //Uses cstddef and cstdlib
    StringVar::StringVar(int size) : max_length(size)
    {
        value = new char[max_length + 1];
        value[0] = '\0';
    }
    
    //Uses cstddef and cstdlib
    StringVar::StringVar() : max_length(100)
    {
        value = new char[max_length + 1];
        value[0] = '\0';
    }
    
    // Uses cstring, cstddef and cstdlib
    StringVar::StringVar(const char a[]) : max_length(strlen(a))
    {
        value = new char[max_length + 1];
        
        for(int i=0; i<strlen(a); i++)
        {
            value[i] = a[i];
        }
        value[strlen(a)]='\0';
    }
    
    //Uses cstring, cstddef and cstdlib
    StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
    {
        value = new char[max_length + 1];
        for(int i=0; i<strlen(string_object.value); i++)
        {
            value[i] = string_object.value[i];
        }
        value[strlen(string_object.value)]='\0';
    }
    
    StringVar::~StringVar()
    {
        delete [] value;
    }
    
    //Uses cstring
    int StringVar::length() const
    {
        return strlen(value);
    }
    
    //Uses iostream
    void StringVar::input_line(istream& ins)
    {
        ins.getline(value, max_length + 1);
    }
    
    //Uses iostream
    ostream& operator << (ostream& outs, const StringVar& the_string)
    {
        outs << the_string.value;
        return outs;
    }
    istream& operator >> (istream& input,const StringVar& the_string)
    {
        input >> the_string.value;
        return input;
    }
    char StringVar::char_one(int posi)
    {
        
        return value[posi-1];
    }
    void StringVar::set_char(int posi, char replace)
    {
        value[posi-1] = replace;
        
        
    }
    string StringVar::copy_piece(int posi,int end)
    {
        int i;
        string copy={};
        for (i=posi-1; i<=end; i++) {
            
            copy += value[i];
        }
        
        return copy;
    }
    bool operator==(StringVar s1, StringVar s2)
    {
        int n;
        if (s1.length() != s2.length()) {
            cout<<"False"<<endl;
            return false;
        }
        else {
            if (s1.length() > s2.length())
                n = s1.length();
            else
                n = s2.length();
            
            
            for (int i = 0; i < n; i++)
            {
                if (s1.value[i] != s2.value[i])
                {
                    cout<<"False"<<endl;
                    return false;
                }
            }
            cout << "True"<<endl;
            return true;
        }
    }
    StringVar operator + (StringVar s1, StringVar s2)
    {
        StringVar name(s1.max_length + s2.max_length);
        
        for (int i = 0; i < s1.max_length; i++) {
            name.value[i] = s1.value[i];
        }
        int counter = 0;
        for (int j = s1.max_length; j < s2.max_length + s1.max_length; j++) {
            name.value[j] = s2.value[counter];
            counter++;
        }
        name.value[s1.max_length + s2.max_length] = '\0';
        return name;
        
    }
    
}//strvarken


